FROM google/cloud-sdk:384.0.1-alpine

ENV KUBECTL_LATEST_VERSION="v1.23.5"
ENV HELM_VERSION="v3.8.2"
ENV ARGOCD_VERSION="v2.3.3"
ENV KUBESEAL_VERSION="0.17.5"
ENV YQ_VERSION=v4.6.3

RUN apk add --no-cache ca-certificates bash git openssh curl \
    && wget -q https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_LATEST_VERSION}/bin/linux/amd64/kubectl -O /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl

RUN wget -q https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/local/bin/helm \
    && chmod +x /usr/local/bin/helm 

RUN wget https://github.com/argoproj/argo-cd/releases/download/${ARGOCD_VERSION}/argocd-linux-amd64 -O /usr/local/bin/argocd && \
    chmod +x /usr/local/bin/argocd

RUN apk add --update \
	ca-certificates \
	groff \
	less \
	python3 \
	py3-pip \
    jq \
	&& rm -rf /var/cache/apk/* \
    && pip install pip --upgrade \
    && pip install awscli

ENV BUILD_DEPS="gettext"  \
    RUNTIME_DEPS="libintl"

RUN set -x && \
    apk add --update $RUNTIME_DEPS && \
    apk add --virtual build_deps $BUILD_DEPS &&  \
    cp /usr/bin/envsubst /usr/local/bin/envsubst && \
    apk del build_deps
    
RUN helm plugin install https://github.com/futuresimple/helm-secrets

# Install kubeseal
RUN curl -sLO https://github.com/bitnami-labs/sealed-secrets/releases/download/v${KUBESEAL_VERSION}/kubeseal-${KUBESEAL_VERSION}-linux-amd64.tar.gz && \
    tar xvzf kubeseal-${KUBESEAL_VERSION}-linux-amd64.tar.gz && \
    mv kubeseal /usr/bin/kubeseal && \
    chmod +x /usr/bin/kubeseal

RUN wget https://github.com/mikefarah/yq/releases/download/${YQ_VERSION}/yq_linux_amd64 -O /usr/bin/yq &&\
    chmod +x /usr/bin/yq && \
    yq --version

